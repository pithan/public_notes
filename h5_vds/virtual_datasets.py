import h5py
import numpy
import os

# in this script virtual datasets are used.
# Virtual dataset files cannot be opened with
# versions of the hdf5 library older than 1.10.

# Specs of the test system:
# ~ 'Summary of the h5py configuration'
# ~ '---------------------------------'
# ~
# ~ 'h5py    2.9.0'
# ~ 'HDF5    1.10.4'
# ~ 'Python  3.7.4 (default, Aug 13 2019, 20:35:49) '
# ~ '[GCC 7.3.0]'
# ~ 'sys.platform    linux'
# ~ 'sys.maxsize     9223372036854775807'
# ~ 'numpy   1.17.2'

with h5py.File("test_virtual_datasets.h5") as f:

    # creation of two simple entries
    f["1"] = numpy.arange(0, 10).astype(numpy.float)
    f["2"] = numpy.arange(10, 20).astype(numpy.float)

    my_dtype = f["2"].dtype

    ###############################################################################

    # creation of a virtual dataset (VDS) containing the two datasets
    # defined above using the native h5py route to procuce VDS directly from
    # a dataset. This does not work once correctly when used inside the same
    # file. Just renaming the file is sufficient to break the links in the virtual
    # dataset
    layout_1 = h5py.VirtualLayout(shape=(20,), dtype=my_dtype)
    vsource1 = h5py.VirtualSource(f["1"])
    vsource2 = h5py.VirtualSource(f["2"])

    layout_1[0:10] = vsource1
    layout_1[10:20] = vsource2

    f.create_virtual_dataset(
        "3_VDS_not_correctly_working", layout_1, fillvalue=numpy.nan
    )
    # if things worked you should see a dataset containing the numbers 0 to 19 and no `nan`.
    # Try renaming the file in the file system and open it again to see how these links break.

    ######################################################################################

    # creation of a virtual dataset (VDS) containing the two datasets
    # defined above specifing names instead of passing the datasets directly
    # This seems to work fine without the problems mentioned above
    layout_2 = h5py.VirtualLayout(shape=(20,), dtype=my_dtype)
    vsource3 = h5py.VirtualSource(".", name="1", shape=f["1"].shape)
    vsource4 = h5py.VirtualSource(".", name="2", shape=f["2"].shape)

    layout_2[0:10] = vsource3
    layout_2[10:20] = vsource4

    f.create_virtual_dataset("4_VDS_working", layout_2, fillvalue=numpy.nan)
    # seems like the problem in the first examlple is rather located in h5py than hdf5.

# move the file to highlight the problem
os.rename("test_virtual_datasets.h5", "test_virtual_datasets_moved.h5")
