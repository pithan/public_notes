Virtual Datasets linking within the same h5 file
------------------------------------------------

When trying to create virtual datasets through h5py one encounters some problems when linking within the same file. Here, the following [example](virtual_datasets.py) is used to highlight these problems and to present workaround: (tested using h5py 2.9.0 and HDF5 1.10.4):

```python
import h5py
import numpy
import os

# in this script virtual datasets are used.
# Virtual dataset files cannot be opened with
# versions of the hdf5 library older than 1.10.

# Specs of the test system:
# ~ 'Summary of the h5py configuration'
# ~ '---------------------------------'
# ~
# ~ 'h5py    2.9.0'
# ~ 'HDF5    1.10.4'
# ~ 'Python  3.7.4 (default, Aug 13 2019, 20:35:49) '
# ~ '[GCC 7.3.0]'
# ~ 'sys.platform    linux'
# ~ 'sys.maxsize     9223372036854775807'
# ~ 'numpy   1.17.2'

with h5py.File("test_virtual_datasets.h5") as f:

    # creation of two simple entries
    f["1"] = numpy.arange(0, 10).astype(numpy.float)
    f["2"] = numpy.arange(10, 20).astype(numpy.float)

    my_dtype = f["2"].dtype

    ###############################################################################

    # creation of a virtual dataset (VDS) containing the two datasets
    # defined above using the native h5py route to procuce VDS directly from
    # a dataset. This does not work once correctly when used inside the same
    # file. Just renaming the file is sufficient to break the links in the virtual
    # dataset
    layout_1 = h5py.VirtualLayout(shape=(20,), dtype=my_dtype)
    vsource1 = h5py.VirtualSource(f["1"])
    vsource2 = h5py.VirtualSource(f["2"])

    layout_1[0:10] = vsource1
    layout_1[10:20] = vsource2

    f.create_virtual_dataset(
        "3_VDS_not_correctly_working", layout_1, fillvalue=numpy.nan
    )
    # if things worked you should see a dataset containing the numbers 0 to 19 and no `nan`.
    # Try renaming the file in the file system and open it again to see how these links break.

    ######################################################################################

    # creation of a virtual dataset (VDS) containing the two datasets
    # defined above specifing names instead of passing the datasets directly
    # This seems to work fine without the problems mentioned above
    layout_2 = h5py.VirtualLayout(shape=(20,), dtype=my_dtype)
    vsource3 = h5py.VirtualSource(".", name="1", shape=f["1"].shape)
    vsource4 = h5py.VirtualSource(".", name="2", shape=f["2"].shape)

    layout_2[0:10] = vsource3
    layout_2[10:20] = vsource4

    f.create_virtual_dataset("4_VDS_working", layout_2, fillvalue=numpy.nan)
    # seems like the problem in the first examlple is rather located in h5py than hdf5.

# move the file to highlight the problem
os.rename("test_virtual_datasets.h5", "test_virtual_datasets_moved.h5")
```

Opening the freshly created [file](test_virtual_datasets_moved.h5) with e.g. silx view one finds the virtual dataset `3_VDS_not_correctly_working` does not contain any data but only `nan`. On the contrary the entry `4_VDS_working` links to the data as expected (see dump below produced with `h5dump`). 


```python
HDF5 "test_virtual_datasets_moved.h5" {
GROUP "/" {
   DATASET "1" {
      DATATYPE  H5T_IEEE_F64LE
      DATASPACE  SIMPLE { ( 10 ) / ( 10 ) }
      DATA {
      (0): 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
      }
   }
   DATASET "2" {
      DATATYPE  H5T_IEEE_F64LE
      DATASPACE  SIMPLE { ( 10 ) / ( 10 ) }
      DATA {
      (0): 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
      }
   }
   DATASET "3_VDS_not_correctly_working" {
      DATATYPE  H5T_IEEE_F64LE
      DATASPACE  SIMPLE { ( 20 ) / ( 20 ) }
      DATA {
      (0): nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan,
      (13): nan, nan, nan, nan, nan, nan, nan
      }
   }
   DATASET "4_VDS_working" {
      DATATYPE  H5T_IEEE_F64LE
      DATASPACE  SIMPLE { ( 20 ) / ( 20 ) }
      DATA {
      (0): 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
      (19): 19
      }
   }
}
}
```

Inspecting in further dephs (`h5dump -p`, see below) clearly shows that during the creation of `3_VDS_not_correctly_working` using `h5py.VirtualSource( DATASET )` absolute file links containing the name of the h5-file itself are put in place (as one would expect for external file links). The hdf5 api however offers also the possibility to use relative links using `.` as done for `4_VDS_working` via `h5py.VirtualSource(".", name=DATASET_NAME, shape=DATASET_SHAPE)`

```python
HDF5 "test_virtual_datasets_moved.h5" {
GROUP "/" {

   ...

   DATASET "3_VDS_not_correctly_working" {
      DATATYPE  H5T_IEEE_F64LE
      DATASPACE  SIMPLE { ( 20 ) / ( 20 ) }
      STORAGE_LAYOUT {
         MAPPING 0 { 
            VIRTUAL {
               SELECTION REGULAR_HYPERSLAB { 
                  START (0)
                  STRIDE (1)
                  COUNT (1)
                  BLOCK (10)
               }
            }
            SOURCE {
               FILE "test_virtual_datasets.h5"
               DATASET "/1"
               SELECTION ALL
            }
         }
         MAPPING 1 { 
            VIRTUAL {
               SELECTION REGULAR_HYPERSLAB { 
                  START (10)
                  STRIDE (1)
                  COUNT (1)
                  BLOCK (10)
               }
            }
            SOURCE {
               FILE "test_virtual_datasets.h5"
               DATASET "/2"
               SELECTION ALL
            }
         }
      }
      FILLVALUE {
         FILL_TIME H5D_FILL_TIME_IFSET
         VALUE  nan
      }
      DATA {
      (0): nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan,
      (13): nan, nan, nan, nan, nan, nan, nan
      }
   }
   DATASET "4_VDS_working" {
      DATATYPE  H5T_IEEE_F64LE
      DATASPACE  SIMPLE { ( 20 ) / ( 20 ) }
      STORAGE_LAYOUT {
         MAPPING 0 { 
            VIRTUAL {
               SELECTION REGULAR_HYPERSLAB { 
                  START (0)
                  STRIDE (1)
                  COUNT (1)
                  BLOCK (10)
               }
            }
            SOURCE {
               FILE "."
               DATASET "1"
               SELECTION ALL
            }
         }
         MAPPING 1 { 
            VIRTUAL {
               SELECTION REGULAR_HYPERSLAB { 
                  START (10)
                  STRIDE (1)
                  COUNT (1)
                  BLOCK (10)
               }
            }
            SOURCE {
               FILE "."
               DATASET "2"
               SELECTION ALL
            }
         }
      }
      FILLVALUE {
         FILL_TIME H5D_FILL_TIME_IFSET
         VALUE  nan
      }
      DATA {
      (0): 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
      (19): 19
      }
   }
}
}
```
