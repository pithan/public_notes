Is there a the inverse of a NXsubentry in Nexus?
------------------------------------------------

When using a NeXus compatilbe hdf5 to save the data collected by bliss
there might be a need to group several scans a posteriori. Usecases could be:

- Sequences of scans e.g. in tomo
- Bundeling of calibration scans and real measurement

[Here](nx_groupentry.py) I propose a `NXgroupentry`. It does not exitst today and I thougt about it because I could not find a suitable equivalent in the existing nexus classes.

![image](Screenshot.png)


```python

with h5py.File("groupentry.h5") as f:

    f.create_group("29_group")
    f["29_group"].attrs["NX_class"] = "NXgroupentry"

    f.create_group("29_group/entries")
    f["29_group/entries"].attrs["NX_class"] = "NXcollection"

    f["29_group/entries/26_loopscan"] = f["26_loopscan"]
    f["29_group/entries/27_loopscan"] = f["27_loopscan"]

    f.create_group("29_group/NXapplication_etc.")
    f.create_group("29_group/scan_meta")

    ######## if suitable even a virtual dataset could be used

    f.create_group("30_group")
    f["30_group"].attrs["NX_class"] = "NXgroupentry"

    f.create_group("30_group/entries")
    f["30_group/entries"].attrs["NX_class"] = "NXcollection"

    f["30_group/entries/26_loopscan"] = f["26_loopscan"]
    f["30_group/entries/27_loopscan"] = f["27_loopscan"]

    f.create_group("30_group/NXapplication_etc.")
    f.create_group("30_group/scan_meta")

    entry1 = "26_loopscan/measurement/lima_simulator:roi_counters:r1_sum"
    entry2 = "27_loopscan/measurement/lima_simulator:roi_counters:r1_sum"

    my_dtype = f[entry1].dtype
    layout = h5py.VirtualLayout(shape=(6,), dtype=my_dtype)
    vsource1 = h5py.VirtualSource(".", name=entry1, shape=(1,) + f[entry1].shape)
    vsource2 = h5py.VirtualSource(".", name=entry2, shape=(1,) + f[entry2].shape)

    layout[0:1] = vsource1
    layout[1:6] = vsource2

    f.create_group("30_group/measurement")
    f["30_group/measurement"].attrs["NX_class"] = "NXdata"
    f["30_group/measurement"].attrs["signal"] = "lima_simulator:roi_counters:r1_sum"

    f.create_virtual_dataset(
        "30_group/measurement/lima_simulator:roi_counters:r1_sum",
        layout,
        fillvalue=numpy.nan,
    )
    f["30_group"].attrs["default"] = "measurement"
```
