import h5py
import numpy
from shutil import copyfile

# Specs of the test system:
# ~ 'Summary of the h5py configuration'
# ~ '---------------------------------'
# ~
# ~ 'h5py    2.9.0'
# ~ 'HDF5    1.10.4'
# ~ 'Python  3.7.4 (default, Aug 13 2019, 20:35:49) '
# ~ '[GCC 7.3.0]'
# ~ 'sys.platform    linux'
# ~ 'sys.maxsize     9223372036854775807'
# ~ 'numpy   1.17.2'

copyfile("bliss_data.h5", "groupentry.h5")

## Proposal for a `NXgroupentry` (that doesn't exist to my knowledge)

with h5py.File("groupentry.h5") as f:

    f.create_group("29_group")
    f["29_group"].attrs["NX_class"] = "NXgroupentry"

    f.create_group("29_group/entries")
    f["29_group/entries"].attrs["NX_class"] = "NXcollection"

    f["29_group/entries/26_loopscan"] = f["26_loopscan"]
    f["29_group/entries/27_loopscan"] = f["27_loopscan"]

    f.create_group("29_group/NXapplication_etc.")
    f.create_group("29_group/scan_meta")

    ######## if suitable even a virtual dataset could be used

    f.create_group("30_group")
    f["30_group"].attrs["NX_class"] = "NXgroupentry"

    f.create_group("30_group/entries")
    f["30_group/entries"].attrs["NX_class"] = "NXcollection"

    f["30_group/entries/26_loopscan"] = f["26_loopscan"]
    f["30_group/entries/27_loopscan"] = f["27_loopscan"]

    f.create_group("30_group/NXapplication_etc.")
    f.create_group("30_group/scan_meta")

    entry1 = "26_loopscan/measurement/lima_simulator:roi_counters:r1_sum"
    entry2 = "27_loopscan/measurement/lima_simulator:roi_counters:r1_sum"

    my_dtype = f[entry1].dtype
    layout = h5py.VirtualLayout(shape=(6,), dtype=my_dtype)
    vsource1 = h5py.VirtualSource(".", name=entry1, shape=(1,) + f[entry1].shape)
    vsource2 = h5py.VirtualSource(".", name=entry2, shape=(1,) + f[entry2].shape)

    layout[0:1] = vsource1
    layout[1:6] = vsource2

    f.create_group("30_group/measurement")
    f["30_group/measurement"].attrs["NX_class"] = "NXdata"
    f["30_group/measurement"].attrs["signal"] = "lima_simulator:roi_counters:r1_sum"

    f.create_virtual_dataset(
        "30_group/measurement/lima_simulator:roi_counters:r1_sum",
        layout,
        fillvalue=numpy.nan,
    )
    f["30_group"].attrs["default"] = "measurement"
